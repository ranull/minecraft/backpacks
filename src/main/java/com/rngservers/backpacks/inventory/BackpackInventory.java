package com.rngservers.backpacks.inventory;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

import java.util.UUID;

public class BackpackInventory implements InventoryHolder {
    private Inventory inventory;
    private UUID uuid;

    public BackpackInventory(String name, Integer size, UUID uuid) {
        inventory = Bukkit.createInventory(this, size, ChatColor.RESET + name);
        this.uuid = uuid;
    }

    @Override
    public Inventory getInventory() {
        return inventory;
    }

    public UUID getUUID() {
        return uuid;
    }
}
