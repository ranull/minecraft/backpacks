package com.rngservers.backpacks.backpack;

import com.rngservers.backpacks.Backpacks;
import com.rngservers.backpacks.inventory.BackpackInventory;
import org.bukkit.ChatColor;
import org.bukkit.NamespacedKey;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

public class BackpackManager {
    private Backpacks plugin;

    public BackpackManager(Backpacks plugin) {
        this.plugin = plugin;
    }

    public boolean isBackpack(ItemStack itemStack) {
        if (itemStack != null && itemStack.hasItemMeta()) {
            return itemStack.getItemMeta().getPersistentDataContainer().has(new NamespacedKey(plugin, "backpack"), PersistentDataType.INTEGER);
        }

        return false;
    }

    public UUID getUUID(ItemStack itemStack) {
        NamespacedKey key = new NamespacedKey(plugin, "backpackUUID");

        if (itemStack.hasItemMeta() && Objects.requireNonNull(itemStack.getItemMeta())
                .getPersistentDataContainer().has(key, PersistentDataType.STRING)) {
            return UUID.fromString(Objects.requireNonNull(itemStack.getItemMeta().getPersistentDataContainer().get(key, PersistentDataType.STRING)));
        }

        return null;
    }

    public void setUUID(ItemStack itemStack, UUID uuid) {
        if (itemStack != null) {
            ItemMeta itemMeta = itemStack.getItemMeta();

            if (itemMeta != null) {
                NamespacedKey key = new NamespacedKey(plugin, "backpackUUID");
                itemMeta.getPersistentDataContainer().set(key, PersistentDataType.STRING, uuid.toString());

                itemStack.setItemMeta(itemMeta);
            }
        }
    }

    public boolean isSaving(ItemStack itemStack) {
        return itemStack.hasItemMeta() && Objects.requireNonNull(itemStack.getItemMeta())
                .getPersistentDataContainer().has(new NamespacedKey(plugin, "backpackSaving"),
                        PersistentDataType.INTEGER);
    }

    public void setSaving(ItemStack itemStack, boolean value) {
        if (itemStack != null) {
            ItemMeta itemMeta = itemStack.getItemMeta();

            if (itemMeta != null) {
                NamespacedKey key = new NamespacedKey(plugin, "backpackSaving");

                if (value) {
                    itemMeta.getPersistentDataContainer().set(key, PersistentDataType.INTEGER, 1);
                } else {
                    itemMeta.getPersistentDataContainer().remove(key);
                }

                itemStack.setItemMeta(itemMeta);
            }
        }
    }

    public String getName(ItemStack itemStack) {
        if (itemStack.hasItemMeta() && Objects.requireNonNull(itemStack.getItemMeta()).hasDisplayName()) {
            return itemStack.getItemMeta().getDisplayName();
        } else {
            return "Backpack";
        }
    }

    public int getSize(ItemStack itemStack) {
        if (itemStack.hasItemMeta()) {
            NamespacedKey key = new NamespacedKey(plugin, "backpackSize");

            if (Objects.requireNonNull(itemStack.getItemMeta()).getPersistentDataContainer().has(key,
                    PersistentDataType.INTEGER)) {
                return itemStack.getItemMeta().getPersistentDataContainer().get(key, PersistentDataType.INTEGER);
            }
        }

        return 0;
    }

    public void setSlotData(ItemStack itemStack, int slot, String slotData) {
        if (itemStack != null) {
            ItemMeta meta = itemStack.getItemMeta();
            NamespacedKey key = new NamespacedKey(plugin, "backpackSlot" + slot);
            meta.getPersistentDataContainer().set(key, PersistentDataType.STRING, slotData);
            itemStack.setItemMeta(meta);
        }
    }

    public ItemStack findBackpack(Player player, UUID uuid) {
        List<ItemStack> backpacks = new ArrayList<>();
        for (ItemStack itemStack : player.getInventory().getContents()) {
            if (itemStack != null && isBackpack(itemStack)) {
                if (getUUID(itemStack).equals(uuid)) {
                    backpacks.add(itemStack);
                }
            }
        }
        if (backpacks.size() > 1) {
            int backpackAmount = backpacks.size();
            int count = 0;
            for (ItemStack backpack : backpacks) {
                if (count >= backpackAmount - 1) {
                    return backpack;
                }
                setUUID(backpack, UUID.randomUUID());
                count++;
            }
        }
        if (!backpacks.isEmpty()) {
            return backpacks.get(0);
        }
        return null;
    }

    public boolean openBackpack(Player player, ItemStack itemStack) {
        if (isBackpack(itemStack)) {
            BackpackInventory backpackInventory = createBackpack(itemStack);
            if (backpackInventory != null) {
                player.openInventory(backpackInventory.getInventory());
                backpackOpenSound(player);
                return true;
            }
        }
        return false;
    }

    public BackpackInventory createBackpack(ItemStack itemStack) {
        BackpackInventory backpackInventory = new BackpackInventory(getName(itemStack), getSize(itemStack), getUUID(itemStack));
        if (itemStack.hasItemMeta()) {
            for (int i = 0; i < backpackInventory.getInventory().getSize(); i++) {
                try {
                    String slotData = getSlotData(itemStack, i);
                    if (slotData != null) {
                        BukkitObjectInputStream data = new BukkitObjectInputStream(new ByteArrayInputStream(Base64.getDecoder().decode(slotData)));
                        backpackInventory.getInventory().setItem(i, (ItemStack) data.readObject());
                    }
                } catch (IOException | ClassNotFoundException exception) {
                    exception.printStackTrace();
                }
            }
        }
        return backpackInventory;
    }

    public void saveBackpack(ItemStack itemStack, BackpackInventory backpackInventory) {
        for (int i = 0; i < backpackInventory.getInventory().getSize(); i++) {
            String slotData = getSlotData(backpackInventory, i);
            setSlotData(itemStack, i, slotData);
        }
    }

    public String getSlotData(ItemStack itemStack, int slot) {
        NamespacedKey key = new NamespacedKey(plugin, "backpackSlot" + slot);
        if (itemStack.hasItemMeta() || itemStack.getItemMeta().getPersistentDataContainer().has(key, PersistentDataType.STRING)) {
            return itemStack.getItemMeta().getPersistentDataContainer().get(key, PersistentDataType.STRING);
        }
        return null;
    }

    public String getSlotData(BackpackInventory backpackInventory, int slot) {
        try {
            ByteArrayOutputStream slotData = new ByteArrayOutputStream();
            BukkitObjectOutputStream data = new BukkitObjectOutputStream(slotData);
            data.writeObject(backpackInventory.getInventory().getItem(slot));
            data.close();
            return Base64.getEncoder().encodeToString(slotData.toByteArray());
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public ItemStack generateBackpack(ItemStack itemStack, int size, String name, String texture) {
        addSkullTexture(itemStack, texture);
        return generateBackpack(itemStack, size, name);
    }

    public ItemStack generateBackpack(ItemStack itemStack, int size, String name) {
        if (isBackpack(itemStack)) {
            return itemStack;
        }
        ItemMeta meta = itemStack.getItemMeta();
        meta.setDisplayName(ChatColor.RESET + name.replace("&", "§"));
        meta.getPersistentDataContainer().set(new NamespacedKey(plugin, "backpack"), PersistentDataType.INTEGER, 1);
        meta.getPersistentDataContainer().set(new NamespacedKey(plugin, "backpackUUID"), PersistentDataType.STRING, UUID.randomUUID().toString());
        meta.getPersistentDataContainer().set(new NamespacedKey(plugin, "backpackSize"), PersistentDataType.INTEGER, getBackpackSize(size));
        itemStack.setItemMeta(meta);
        return itemStack;
    }

    public int getBackpackSize(int size) {
        switch (size) {
            case 1:
                return 9;
            case 2:
                return 18;
            case 3:
                return 27;
            case 4:
                return 36;
            case 5:
                return 45;
            default:
                return 54;
        }
    }

    public void backpackOpenSound(Player player) {
        player.playSound(player.getLocation(), Sound.ENTITY_ENDER_DRAGON_FLAP, 1, 2);
    }

    @SuppressWarnings("deprecation")
    public void addSkullTexture(ItemStack item, String base64) {
        if (!base64.equals("")) {
            UUID hashAsId = new UUID(base64.hashCode(), base64.hashCode());
            plugin.getServer().getUnsafe().modifyItemStack(item, "{SkullOwner:{Id:\"" + hashAsId + "\",Properties:{textures:[{Value:\"" + base64 + "\"}]}}}");
        }
    }
}
