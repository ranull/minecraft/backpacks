package com.rngservers.backpacks.recipe;

import com.rngservers.backpacks.Backpacks;
import com.rngservers.backpacks.backpack.BackpackManager;
import org.bukkit.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class RecipeManager {
    private Backpacks plugin;
    private BackpackManager backpackManager;

    public RecipeManager(Backpacks plugin, BackpackManager backpackManager) {
        this.plugin = plugin;
        this.backpackManager = backpackManager;
    }

    public void loadRecipes() {
        for (String key : Objects
                .requireNonNull(plugin.getConfig().getConfigurationSection("settings.backpacks"))
                .getKeys(false)) {
            System.out.println("Added [" + key + "] recipe");
            createBackpackRecipe(key);
        }
    }

    public void unloadRecipes() {
        Iterator<Recipe> recipes = plugin.getServer().recipeIterator();

        while (recipes.hasNext()) {
            Recipe recipe = recipes.next();

            if (recipe != null) {
                ItemStack item = recipe.getResult();
                if (backpackManager.isBackpack(item)) {
                    recipes.remove();
                }
            }
        }
    }

    public void createBackpackRecipe(String id) {
        Material backpackMaterial = Material.matchMaterial(Objects
                .requireNonNull(plugin.getConfig().getString("settings.backpacks." + id + ".material")));

        if (backpackMaterial != null) {
            ItemStack itemStack = new ItemStack(backpackMaterial, 1);
            String texture = plugin.getConfig().getString("settings.backpacks." + id + ".texture");
            String name = Objects.requireNonNull(plugin.getConfig()
                    .getString("settings.backpacks." + id + ".name"))
                    .replace("&", "§");

            int size = plugin.getConfig().getInt("settings.backpacks." + id + ".size");
            backpackManager.generateBackpack(itemStack, size, name, texture);

            NamespacedKey key = new NamespacedKey(plugin, "backpack_" + id + "_recipe");
            ShapedRecipe recipe = new ShapedRecipe(key, itemStack);

            recipe.shape("ABC", "DEF", "GHI");

            List<String> lines = plugin.getConfig().getStringList("settings.backpacks." + id + ".recipe");

            Integer recipeKey = 1;

            for (String string : lines.get(0).split(" ")) {
                Material material = Material.matchMaterial(string);

                if (material != null) {
                    recipe.setIngredient(getChar(recipeKey), material);
                }

                recipeKey++;
            }
            for (String string : lines.get(1).split(" ")) {
                Material material = Material.matchMaterial(string);

                if (material != null) {
                    recipe.setIngredient(getChar(recipeKey), material);
                }

                recipeKey++;
            }

            for (String string : lines.get(2).split(" ")) {
                Material material = Material.matchMaterial(string);

                if (material != null) {
                    recipe.setIngredient(getChar(recipeKey), material);
                }

                recipeKey++;
            }

            plugin.getServer().addRecipe(recipe);
        }
    }

    public char getChar(Integer count) {
        switch (count) {
            case 1:
                return 'A';
            case 2:
                return 'B';
            case 3:
                return 'C';
            case 4:
                return 'D';
            case 5:
                return 'E';
            case 6:
                return 'F';
            case 7:
                return 'G';
            case 8:
                return 'H';
            case 9:
                return 'I';
            default:
                return '*';
        }
    }
}
