package com.rngservers.backpacks.events;

import com.rngservers.backpacks.Backpacks;
import com.rngservers.backpacks.backpack.BackpackManager;
import com.rngservers.backpacks.inventory.BackpackInventory;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Objects;

public class Events implements Listener {
    private Backpacks plugin;
    private BackpackManager backpackManager;

    public Events(Backpacks plugin, BackpackManager backpackManager) {
        this.plugin = plugin;
        this.backpackManager = backpackManager;
    }

    @EventHandler
    public void onPlayerClick(PlayerInteractEvent event) {
        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && !event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
            return;
        }

        Player player = event.getPlayer();

        ItemStack mainHand = player.getInventory().getItemInMainHand();
        ItemStack offHand = player.getInventory().getItemInOffHand();
        ItemStack backpackItem = null;

        if (backpackManager.isBackpack(mainHand) && event.getHand() == EquipmentSlot.HAND) {
            backpackItem = mainHand;
        } else if (backpackManager.isBackpack(offHand) && event.getHand() == EquipmentSlot.OFF_HAND) {
            backpackItem = offHand;
        }

        if (backpackItem != null) {
            if (backpackItem.getAmount() > 1) {
                backpackItem.setAmount(1);
            }

            if (!backpackManager.isSaving(backpackItem)) {
                backpackManager.openBackpack(player, backpackItem);
            }

            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryClose(InventoryCloseEvent event) {
        if (event.getInventory().getHolder() instanceof BackpackInventory) {
            Player player = (Player) event.getPlayer();

            BackpackInventory backpackInventory = (BackpackInventory) event.getInventory().getHolder();

            ItemStack backpackItem = backpackManager.findBackpack(player, backpackInventory.getUUID());

            if (backpackItem != null) {
                backpackManager.saveBackpack(backpackItem, backpackInventory);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.isCancelled()) {
            return;
        }

        Player player = (Player) event.getWhoClicked();
        ItemStack itemStack = null;

        if (event.getCurrentItem() != null) {
            itemStack = event.getCurrentItem();
        } else if (event.getCursor() != null) {
            itemStack = event.getCursor();
        }

        if (itemStack != null && event.getInventory().getHolder() != null) {
            if (event.getInventory().getHolder() instanceof BackpackInventory) {
                if (event.getClick().equals(ClickType.NUMBER_KEY)) {
                    event.setCancelled(true);
                }

                if (!plugin.getConfig().getBoolean("settings.backpackInBackpack")) {
                    if (backpackManager.isBackpack(itemStack)) {
                        event.setCancelled(true);
                        return;
                    }
                }

                if (event.getClickedInventory() != null &&
                        !(event.getClickedInventory().getHolder() instanceof BackpackInventory)) {
                    if (plugin.getConfig().getStringList("settings.blacklistItems").contains(itemStack.getType()
                            .toString())) {
                        event.setCancelled(true);
                        return;
                    }
                }

                BackpackInventory backpackInventory = (BackpackInventory) event.getInventory().getHolder();
                ItemStack backpackItem = backpackManager.findBackpack(player, backpackInventory.getUUID());

                if (itemStack.equals(backpackItem)) {
                    event.setCancelled(true);
                    return;
                }

                if (backpackItem != null) {
                    if (itemStack.getType() != Material.AIR) {
                        backpackManager.setSaving(backpackItem, true);
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                backpackManager.saveBackpack(backpackItem, backpackInventory);
                                backpackManager.setSaving(backpackItem, false);
                            }
                        }.runTaskLater(plugin, 1L);
                    }
                } else {
                    event.setCancelled(true);
                    player.closeInventory();
                }
            } else {
                if (event.getClickedInventory() != null) {
                    if (event.getClickedInventory() instanceof PlayerInventory) {
                        if (plugin.getConfig().getStringList("settings.blacklistInventoryType")
                                .contains(event.getClickedInventory().getType().toString()) ||
                                plugin.getConfig().getStringList("settings.blacklistContainers")
                                        .contains(Objects.requireNonNull(event.getClickedInventory()
                                                .getHolder()).toString())) {
                            if (event.getClick().equals(ClickType.NUMBER_KEY)) {
                                event.setCancelled(true);
                            }

                            if (backpackManager.isBackpack(itemStack)) {
                                event.setCancelled(true);
                            }
                        }

                        if (backpackManager.isBackpack(itemStack)) {
                            if (plugin.getConfig().getStringList("settings.blacklistInventoryType")
                                    .contains(event.getInventory().getType().toString()) ||
                                    plugin.getConfig().getStringList("settings.blacklistContainers")
                                            .contains(event.getInventory().getHolder().toString())) {
                                if (backpackManager.isBackpack(itemStack)) {
                                    event.setCancelled(true);
                                }
                            }
                        }
                    } else {
                        if (plugin.getConfig().getStringList("settings.blacklistInventoryType")
                                .contains(event.getInventory().getType().toString()) ||
                                plugin.getConfig().getStringList("settings.blacklistContainers")
                                        .contains(event.getInventory().getHolder().toString())) {
                            if (event.getClick().equals(ClickType.NUMBER_KEY)) {
                                event.setCancelled(true);
                            }
                        }
                    }
                }
            }
        } else {
            if (!plugin.getConfig().getBoolean("settings.allowEnderChest")) {
                if (event.getInventory().equals(player.getEnderChest())) {
                    if (event.getClick().equals(ClickType.NUMBER_KEY)) {
                        event.setCancelled(true);
                    }

                    if (event.getClickedInventory() != null && !(event.getClickedInventory().equals(player.getEnderChest()))) {
                        if (backpackManager.isBackpack(itemStack)) {
                            event.setCancelled(true);
                        }
                    }
                }
            }
        }
    }
}
