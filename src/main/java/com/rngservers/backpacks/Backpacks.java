package com.rngservers.backpacks;

import com.rngservers.backpacks.backpack.BackpackManager;
import com.rngservers.backpacks.commands.BackpacksCommand;
import com.rngservers.backpacks.events.Events;
import com.rngservers.backpacks.recipe.RecipeManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class Backpacks extends JavaPlugin {
    private RecipeManager recipeManager;

    @Override
    public void onEnable() {
        saveDefaultConfig();
        BackpackManager backpackManager = new BackpackManager(this);
        recipeManager = new RecipeManager(this, backpackManager);
        recipeManager.loadRecipes();
        getServer().getPluginManager().registerEvents(new Events(this, backpackManager), this);
        getCommand("backpacks").setExecutor(new BackpacksCommand(this, backpackManager, recipeManager));
    }

    @Override
    public void onDisable() {
        recipeManager.unloadRecipes();
    }
}
