package com.rngservers.backpacks.commands;

import com.rngservers.backpacks.Backpacks;
import com.rngservers.backpacks.backpack.BackpackManager;
import com.rngservers.backpacks.recipe.RecipeManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class BackpacksCommand implements CommandExecutor {
    private Backpacks plugin;
    private BackpackManager backpackManager;
    private RecipeManager recipeManager;

    public BackpacksCommand(Backpacks plugin, BackpackManager backpackManager, RecipeManager recipeManager) {
        this.plugin = plugin;
        this.backpackManager = backpackManager;
        this.recipeManager = recipeManager;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        String version = "1.0";
        String author = "RandomUnknown";

        if (args.length < 1) {
            sender.sendMessage(ChatColor.DARK_GRAY + "» " + ChatColor.DARK_GREEN + "Backpacks " + ChatColor.GRAY
                    + ChatColor.GRAY + "v" + version);
            if (sender.hasPermission("backpacks.reload")) {
                sender.sendMessage(ChatColor.GRAY + "/backpacks reload " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET
                        + " Reload plugin");
            }
            sender.sendMessage(ChatColor.DARK_GRAY + "Author: " + ChatColor.GRAY + author);
            return true;
        }
        if (args.length == 1) {
            if (args[0].toLowerCase().equals("reload")) {
                if (sender.hasPermission("backpacks.reload")) {
                    plugin.reloadConfig();
                    recipeManager.unloadRecipes();
                    recipeManager.loadRecipes();
                    sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.DARK_GREEN + "Backpacks"
                            + ChatColor.DARK_GRAY + "]" + ChatColor.RESET + " Config reloaded!");
                } else {
                    sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.DARK_GREEN + "Backpacks"
                            + ChatColor.DARK_GRAY + "]" + ChatColor.RESET + " No Permission!");
                }
            }
            if (args[0].toLowerCase().equals("give")) {
                if (sender instanceof Player) {
                    Player player = (Player) sender;
                    if (sender.hasPermission("backpacks.give")) {
                        String name = "Small Backpack";
                        String texture = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvODM1MWU1MDU5ODk4MzhlMjcyODdlN2FmYmM3Zjk3ZTc5NmNhYjVmMzU5OGE3NjE2MGMxMzFjOTQwZDBjNSJ9fX0";
                        ItemStack item = new ItemStack(Material.PLAYER_HEAD, 1);
                        backpackManager.generateBackpack(item, 3, name, texture);
                        player.getInventory().addItem(item);
                        sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.DARK_GREEN + "Backpacks"
                                + ChatColor.DARK_GRAY + "]" + ChatColor.RESET + " Given backpack!");
                    } else {
                        sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.DARK_GREEN + "Backpacks"
                                + ChatColor.DARK_GRAY + "]" + ChatColor.RESET + " No Permission!");
                    }
                }
            }
        }
        return true;
    }
}